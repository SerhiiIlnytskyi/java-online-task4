package com.epam.courses.game;

import com.epam.courses.game.model.Artifact;
import com.epam.courses.game.model.Character;
import com.epam.courses.game.model.Location;
import com.epam.courses.game.model.Monster;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Game {
    public static void main(String[] args) throws IOException {
        Game game = new Game();
        game.initGame(10);
        game.printDoorBehinders();
        //TODO change private
        game.character.setStrength(25);
        game.startGame();
    }


    private Character character = Character.getInstance();

    private List<Location> locations = new ArrayList<>();

    public void startGame() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (character.isAlive()) {
            System.out.println("Please choose number of door from 1 to " + (locations.size()));
            Location location = locations.get(Integer.parseInt(reader.readLine()) - 1);
            if (location.isVisited()) {
                System.out.println("You already clear this location");
                continue;
            }
            if (location.getArtifact() != null) {
                findArtifact(location.getArtifact());
                location.setVisited(true);
            }
            if (location.getMonster() != null) {
                battle(location.getMonster());
                location.setVisited(true);
            }
        }
        reader.close();
    }

    public void initGame(int numberOfDoors) {
        for (int i = 0; i < numberOfDoors; i++) {
            Location location;

            Random random = new Random();
            int chooseArtifactOrMonster = random.nextInt(2);

            if (chooseArtifactOrMonster == 0) {
                location = new Location(new Monster(random.nextInt(100)));
            } else {
                location = new Location(new Artifact((random.nextInt(100))));
            }
            locations.add(location);
        }
    }

    private void printDoorBehinders() {
        for (int i = 0; i < locations.size(); i++) {
            Location location = locations.get(i);
            System.out.printf("Door №%d: %23s : %s\n", i,
                    location.isLocationWithMonster() ? location.getMonster() : location.getArtifact(),
                    location.isVisited() ? "Visited" : "Not visited");
        }
    }

    public void findArtifact(Artifact artifact) {
        character.addArtifact(artifact);
    }

    public boolean battle(Monster monster) {
        if (character.getTotalStrength() >= monster.getStrength()) {
            System.out.println("Character won!!!");
            return true;
        } else {
            System.out.println("Monster won =( ");
            character.setAlive(false);
            return false;
        }
    }

    private int calculateDeathLocations(int currentLocation, int deathLocationsCount) {
        if (currentLocation == locations.size()) return deathLocationsCount;
        if ((locations.get(currentLocation).isLocationWithMonster()) &&
                (character.getTotalStrength() < locations.get(currentLocation).getMonster().getStrength())) {
            deathLocationsCount ++;
        }
        return calculateDeathLocations(currentLocation + 1, deathLocationsCount);
    }

}
