package com.epam.courses.game.model;

public class Monster {
    private int strength;

    public Monster(int strength) {
        this.strength = strength;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    @Override
    public String toString() {
        return "Monster  {" +
                "strength=" + strength +
                '}';
    }
}
