package com.epam.courses.game.model;

public class Artifact {

    private int strenght;

    public Artifact(int strenght) {
        this.strenght = strenght;
    }

    public int getStrenght() {
        return strenght;
    }

    public void setStrenght(int strenght) {
        this.strenght = strenght;
    }

    @Override
    public String toString() {
        return "Artifact {" +
                "strenght=" + strenght +
                '}';
    }
}
