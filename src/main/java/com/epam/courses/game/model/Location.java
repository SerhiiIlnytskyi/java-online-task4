package com.epam.courses.game.model;

public class Location {
    private Monster monster;
    private Artifact artifact;

    private boolean visited;

    public Location(Monster monster) {
        this.monster = monster;
    }

    public Location(Artifact artifact) {
        this.artifact = artifact;
    }

    public boolean isLocationWithMonster() {
        if (monster != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public Monster getMonster() {
        return monster;
    }

    public void setMonster(Monster monster) {
        this.monster = monster;
        this.artifact = null;
    }

    public Artifact getArtifact() {
        return artifact;
    }

    public void setArtifact(Artifact artifact) {
        this.artifact = artifact;
        this.monster = null;
    }

    @Override
    public String toString() {
        return "Location{" +
                (monster == null ? "" : "monster=" + monster) +
                (artifact == null ? "" : "artifact=" + artifact) +
                "}";
    }
}
