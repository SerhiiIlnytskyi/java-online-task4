package com.epam.courses.game.model;

import java.util.ArrayList;
import java.util.List;

public class Character {

    private static final Character instance = new Character();

    private int strength;

    private List<Artifact> artifactList = new ArrayList<>();

    private boolean isAlive;

    private Character() {
        this.isAlive = true;
    }

    public static Character getInstance() {
        return instance;
    }

    public void addArtifact(Artifact artifact) {
        artifactList.add(artifact);
    }

    public int getTotalStrength() {
        int totalStrength = this.strength;
        for (int i = 0; i < artifactList.size(); i++) {
            totalStrength += artifactList.get(i).getStrenght();
        }
        return totalStrength;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public List<Artifact> getArtifactList() {
        return artifactList;
    }

    public void setArtifactList(List<Artifact> artifactList) {
        this.artifactList = artifactList;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }
}
