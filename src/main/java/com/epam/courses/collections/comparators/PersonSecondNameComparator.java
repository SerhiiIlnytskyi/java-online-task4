package com.epam.courses.collections.comparators;

import java.util.Comparator;

public class PersonSecondNameComparator implements Comparator<Person> {

    @Override
    public int compare(Person person1, Person person2) {
        return person1.getSecondName().compareTo(person2.getSecondName());
    }
}
