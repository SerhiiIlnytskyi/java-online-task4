package com.epam.courses.collections.comparators;

import java.util.Comparator;

public class Person implements Comparable<Person> {

    private String firstName = "";
    private String secondName = "";

    public Person() {
    }

    public Person(String firstName, String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
    }

    @Override
    public int compareTo(Person person) {
        return this.firstName.compareTo(person.firstName);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                '}';
    }

    private static class BySecondName implements Comparator<Person> {

        @Override
        public int compare(Person person1, Person person2) {
            return person1.getSecondName().compareTo(person2.getSecondName());
        }
    }

}


