package com.epam.courses.collections.comparators;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Person> personList = PersonGenerator.generate(10);
        PersonGenerator.reset();
        Person[] personArray = PersonGenerator.generate(10).toArray(new Person[10]);

        System.out.println();
        System.out.println("List with generated data: \n" + personList);
        System.out.println("Array with generated data: \n" + Arrays.toString(personArray));

        Collections.sort(personList);
        Arrays.sort(personArray);

        System.out.println();
        System.out.println("Sorted list by firstName: \n" + personList);
        System.out.println("Sorted array by firstName: \n" + Arrays.toString(personArray));

        Comparator<Person> personSecondNameComparator = new PersonSecondNameComparator();

        Collections.sort(personList, personSecondNameComparator);
        Arrays.sort(personArray, personSecondNameComparator);

        System.out.println();
        System.out.println("Sorted list by secondName: \n" + personList);
        System.out.println("Sorted array by secondName: \n" + Arrays.toString(personArray));

        Scanner scanner = new Scanner(System.in);
        System.out.print("Please write second name to search in personList: ");
        String secondName = scanner.nextLine();
        Person person = new Person();
        person.setSecondName(secondName);

        int index = Collections.binarySearch(personList, person, personSecondNameComparator);

        System.out.printf("Index of person with second name %s  is: %d\n", secondName, index);

        scanner.close();
    }
}
