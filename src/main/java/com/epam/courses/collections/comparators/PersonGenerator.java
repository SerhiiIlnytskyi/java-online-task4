package com.epam.courses.collections.comparators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PersonGenerator {
    private static final PersonGenerator instance = new PersonGenerator();

    private static final List<String> firstNames = Arrays.asList("Oliver", "George", "Harry", "Jack", "Jacob", "Noah",
            "Charlie", "Muhammad", "Thomas", "Oscar", "William", "James", "Henry", "Leo", "Alfie", "Joshua", "Freddie",
            "Archie", "Ethan", "Isaac", "Alexander", "Joseph", "Edward", "Samuel", "Max", "Daniel", "Arthur", "Lucas");
    private static final List<String> secondNames = Arrays.asList("Smith", "Johnson", "Williams", "Jones", "Brown",
            "Davis", "Miller", "Wilson", "Moore", "Taylor", "Anderson", "Thomas", "Jackson", "White", "Harris",
            "Martin", "Thompson", "Garcia", "Martinez", "Robinson", "Clark", "Rodriguez", "Lewis", "Lee", "Walker");

    private static List<Person> persons = new ArrayList<>();

    private PersonGenerator() {

    }

    public static Person generateOne() {
        Random rand = new Random();
        String randomFirstName = firstNames.get(rand.nextInt(firstNames.size()));
        String randomSecondName = secondNames.get(rand.nextInt(secondNames.size()));

        return new Person(randomFirstName, randomSecondName);
    }

    public static List<Person> generate(int count) {
        if (count > 0) {
            for (int i = 0; i < count; i++) {
                persons.add(generateOne());
            }
        } else {
            System.out.println("Count must be positive value");
        }
        return persons;
    }


    public static void reset() {
        persons = new ArrayList<>();
    }

    public static PersonGenerator getInstance() {
        return instance;
    }
}
