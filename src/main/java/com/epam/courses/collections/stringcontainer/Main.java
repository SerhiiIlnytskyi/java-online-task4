package com.epam.courses.collections.stringcontainer;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        StringContainer stringContainer = new StringContainer();

        long startTime;
        long stopTime;

        startTime = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            stringContainer.addString(String.valueOf(i));
        }
        stopTime = System.currentTimeMillis();
        System.out.printf("Elapsed time for adding 100_000 elements to string container = %d milliseconds \n", stopTime - startTime);

        startTime = System.currentTimeMillis();
        for (int i = 0; i < stringContainer.getSize(); i++) {
            stringContainer.getString(i);
        }
        stopTime = System.currentTimeMillis();
        System.out.printf("Elapsed time for getting 100_000 elements from string container = %d milliseconds \n", stopTime - startTime);

        List<String> listToPerfomanceComparing = new ArrayList<>();

        startTime = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            listToPerfomanceComparing.add(String.valueOf(i));
        }
        stopTime = System.currentTimeMillis();
        System.out.printf("Elapsed time for adding 100_000 elements to array list = %d milliseconds \n", stopTime - startTime);

        startTime = System.currentTimeMillis();
        for (int i = 0; i < listToPerfomanceComparing.size(); i++) {
            listToPerfomanceComparing.get(i);
        }
        stopTime = System.currentTimeMillis();
        System.out.printf("Elapsed time for getting 100_000 elements from array list = %d milliseconds \n", stopTime - startTime);

    }
}
