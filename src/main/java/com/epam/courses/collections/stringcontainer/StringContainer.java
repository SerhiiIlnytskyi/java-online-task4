package com.epam.courses.collections.stringcontainer;

import java.util.Arrays;

public class StringContainer {

    private String[] stringsData = new String[10];

    private int size;

    public StringContainer() {
        this.size = 0;
    }

    public StringContainer(String[] strings) {
        this.size = strings.length;
        this.stringsData = strings;
    }

    public String getString(int index) {
        if (index <= size) {
            return stringsData[index];
        } else {
            return null;
        }
    }

    public void addString(String string) {
        ensureCapacity(size + 1);
        stringsData[size++] = string;
    }

    private void ensureCapacity(int i) {
        int oldCapacity = stringsData.length;
        if (i > oldCapacity) {
            int newCapacity = oldCapacity + (oldCapacity >> 1);
            stringsData = Arrays.copyOf(stringsData, newCapacity);
        }
    }


    public String[] getStringsData() {
        return Arrays.copyOfRange(stringsData, 0, size);
    }

    public void setStringsData(String[] stringsData) {
        this.stringsData = stringsData;
        this.size = stringsData.length;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "StringContainer{" +
                "stringsData=" + Arrays.toString(Arrays.copyOfRange(stringsData, 0, size)) +
                ", size=" + size +
                '}';
    }
}
