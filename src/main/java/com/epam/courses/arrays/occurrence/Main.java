package com.epam.courses.arrays.occurrence;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] array = {29, 17, 12, 23, 41, 32, 19, 2, 5, 29, 29, 5, 5, 5, 12, 12};

        System.out.println(Arrays.toString(deleteMoreThenTwiceRepeated(array)));
    }

    public static int[] deleteMoreThenTwiceRepeated(int[] array) {
        int[] resultedArray = new int[array.length];
        int resultedArrayIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (numberOfAllOccurrences(array, array[i]) <= 2) {
                resultedArray[resultedArrayIndex++] = array[i];
            }
        }
        return Arrays.copyOf(resultedArray, resultedArrayIndex);
    }

    public static int numberOfAllOccurrences(int[] array, int valueToFind) {
        int occurrencesCount = 0;
        for (int i = 0; i < array.length; i++) {
            if (valueToFind == array[i]) {
                occurrencesCount++;
            }
        }
        return occurrencesCount;
    }
}
