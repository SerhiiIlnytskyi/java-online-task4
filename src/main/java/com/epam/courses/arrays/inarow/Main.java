package com.epam.courses.arrays.inarow;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = {29, 17, 17, 17, 17, 12, 23, 41, 41, 32, 19, 2, 5, 29, 29, 5, 5, 5, 12, 12, 15, 15};

        System.out.println(Arrays.toString(removeDuplicatesInaRow(array)));
    }

    private static int findEndOfSequence(int[] array, int startIndex) {
        if ((startIndex > array.length) || (startIndex < 0)) {
            return -1;
        }
        int index = startIndex;

        if (index == array.length -1) {
            return index;
        } else if (array[index] == array[index + 1]) {
            return findEndOfSequence(array, index + 1);
        } else {
            return index;
        }
    }

    public static int[] removeDuplicatesInaRow(int[] array) {
        int[] resultedArray = new int[array.length];
        int resultedArrayIndex = 0;

        int index = 0;
        while (index <= array.length - 1) {
            int endOfSequence = findEndOfSequence(array, index);
            resultedArray[resultedArrayIndex++] = array[index];
            index = endOfSequence + 1;
        }
        return Arrays.copyOf(resultedArray, resultedArrayIndex);
    }
}