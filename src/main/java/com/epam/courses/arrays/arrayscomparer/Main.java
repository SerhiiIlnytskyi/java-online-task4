package com.epam.courses.arrays.arrayscomparer;


import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] firstArray = {17, 12, 23, 41, 32, 19, 2, 5};
        int[] secondArray = {5, 30, 5, 43, 19, 39, 25, 23, 25, 9, 12, 17, 29};

        System.out.println("Present in both: " + Arrays.toString(presentInBoth(firstArray, secondArray)));
        System.out.println("Present in one:  " + Arrays.toString(presentInOne(firstArray, secondArray)));
    }

    public static int[] presentInBoth(int[] first, int[] second) {
        int[] resultedArray = new int[Math.min(first.length, second.length)];
        int currentIndex = 0;

        for (int i = 0; i < first.length; i++) {
            if (contains(Arrays.copyOf(resultedArray, currentIndex), first[i])) {
                continue;
            }
            if (contains(second, first[i])) {
                resultedArray[currentIndex++] = first[i];
            }
        }
        return Arrays.copyOf(resultedArray, currentIndex);
    }

    public static int[] presentInOne(int[] first, int[] second) {
        int[] resultedArray = new int[first.length + second.length];
        int currentIndex = 0;
        currentIndex = getCurrentIndex(second, first, resultedArray, currentIndex);

        currentIndex = getCurrentIndex(first, second, resultedArray, currentIndex);
        return Arrays.copyOf(resultedArray, currentIndex);
    }

    private static int getCurrentIndex(int[] first, int[] second, int[] resultedArray, int currentIndex) {
        for (int i = 0; i < second.length; i++) {
            if (contains(Arrays.copyOf(resultedArray, currentIndex), second[i])) {
                continue;
            }
            if (!contains(first, second[i])) {
                resultedArray[currentIndex++] = second[i];
            }
        }
        return currentIndex;
    }

    public static boolean contains(int[] array, int valueToContain) {
        return indexOf(array, valueToContain, 0) >= 0;
    }

    public static int indexOf(int[] array, int valueToFind, int startIndex) {
        for (int i = startIndex; i < array.length ; i++) {
            if (valueToFind == array[i]) {
                return i;
            }
        }
        return -1;
    }

}
